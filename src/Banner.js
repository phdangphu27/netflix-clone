import React from "react";
import { useState, useEffect } from "react";
import "./Banner.css";
import instance from "./axios";
import requests from "./Requests";

const Banner = () => {
  const [movie, setMovie] = useState([]);

  useEffect(() => {
    // axios({
    //   url: `https://api.themoviedb.org/3${requests.fetchNetflixOriginals}`,
    //   method: "GET",
    // })
    //   .then((res) => {
    //     setMovie(
    //       res.data.results[
    //         Math.floor(Math.random() * res.data.results.length - 1)
    //       ]
    //     );
    //     console.log(res.data.results);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });

    async function fetchData() {
      const request = await instance.get(requests.fetchNetflixOriginals);
      setMovie(
        request.data.results[
          Math.floor(Math.random() * request.data.results.length - 1)
        ]
      );
      return request;
    }
    fetchData();
  }, []);

  const truncate = (string, n) => {
    return string?.length > n ? string.slice(0, n) + "..." : string;
  };

  return (
    <header
      className='banner'
      style={{
        backgroundSize: "cover",
        backgroundImage: `url("https://image.tmdb.org/t/p/original/${movie?.backdrop_path}")`,
        backgroundPosition: "center",
      }}
    >
      <div className='banner__contents'>
        <h1 className='banner__title'>
          {movie?.title || movie?.name || movie?.original_name}
        </h1>
        <div className='banner__buttons'>
          <button className='banner__button'>PLAY</button>
          <button className='banner__button'>MY LIST</button>
        </div>
        <h1 className='banner__description'>
          {truncate(movie?.overview, 150)}
        </h1>
      </div>

      <div className='banner--fadeBottom' />
    </header>
  );
};

export default Banner;
