import React, { useRef } from "react";
// import { auth } from "../firebase";
import "./SignInScreen.css";

const SignInScreen = () => {
  const emailRef = useRef(null);
  const passwordRef = useRef(null);

  const register = (e) => {
    e.preventDefault();

    // auth
    //   .createUserWithEmailAndPassword(
    //     emailRef.current.value,
    //     passwordRef.current.value
    //   )
    //   .then((authUser) => {
    //     console.log(authUser);
    //   })
    //   .catch((err) => {
    //     alert(err.message);
    //   });
  };

  const signIn = (e) => {
    e.preventDefault();
  };

  return (
    <div>
      <div className='signInScreen'>
        <form>
          <h1>Sign In</h1>
          <input ref={emailRef} placeholder='Email' type='email' />
          <input ref={passwordRef} placeholder='Password' type='password' />
          <button type='submit' onClick={signIn}>
            Sign In
          </button>

          <h4>
            <span className='signInScreen__gray'>New To Netflix?</span>
            <span onClick={register} className='signInScreen__link'>
              Sign Up Now
            </span>
          </h4>
        </form>
      </div>
    </div>
  );
};

export default SignInScreen;
