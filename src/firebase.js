import { initializeApp } from "firebase/app";

import { getFireStore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAjRL0Qtb48Njn0MpLvgIqw2bHOAy3auBU",
  authDomain: "netflix-clone-47d80.firebaseapp.com",
  projectId: "netflix-clone-47d80",
  storageBucket: "netflix-clone-47d80.appspot.com",
  messagingSenderId: "458581537912",
  appId: "1:458581537912:web:51e258d23fd0cd62a9698d",
  measurementId: "G-YVX43F25FL",
};

// const firebaseApp = firebase.initializeApp(firebaseConfig);

const app = initializeApp(firebaseConfig);

const db = getFireStore(app);

// const db = firebase.fireStore();
// const auth = firebase.auth();

// export { auth, db };
